import { Component, OnInit } from '@angular/core';
import { Card } from '../card/card';
import { Cardback } from '../card/cardback';

import { CardService } from '../card/card.service';

@Component({
  selector: 'collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {

  cards:Array<Card>;
  cardbacks:Array<Cardback>;

  constructor(private service:CardService) { }

  ngOnInit() {
    this.service
        .listCards()
        .then(data => {
          this.cards = data;
          console.log(this.cards);
        });
  }

}
