import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Card } from './card';
import { Cardback } from './cardback';
import { PatchInfo } from './patch.info';

import { environment } from '../../environments/environment';

@Injectable()
export class CardService {
  private cardsApi: string = '/cards/classes/Paladin';
  private infoApi: string = '/info';
  private cardbacksApi: string = '/cardbacks';

  private teste: Array<string> = ['Basic', ''];

  private headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'X-Mashape-Key': environment.accessKey
    });
  }

  listCards(): Promise<Array<Card>> {
    return this.http.get(
      environment.api + this.cardsApi, 
      { headers: this.headers })
      .toPromise()
      .then(response => response.json() as Array<Card>)
      .then(cards => cards.filter(c => this.teste.includes(c.cardSet)))
      .catch(e => console.log(e));
  }

  findCard(cardId:string): Promise<Card> {
    return null;
  }

  listCardbacks(): Promise<Array<Cardback>> {
    return null;
  }

  info(): Promise<PatchInfo> {
    return this.http.get(environment.api + this.infoApi, { headers: this.headers })
      .toPromise()
      .then(response => response.json() as PatchInfo)
      .catch(e => console.log(e));
  }
}
