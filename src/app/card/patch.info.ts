export class PatchInfo {
    private patch:string;
    private classes:Array<String>;
    private sets:Array<String>;
    private standard:Array<String>;
    private wild:Array<String>;
    private types:Array<String>;
    private factions:Array<String>;
    private qualities:Array<String>;
    private races:Array<String>;
    
}