export class Card {
    cardId:string;
    name:string;
    cardSet:string;
    type:String;
    rarity:string;
    cost:number;
    attack:number;
    health:number;
    text:string;
    flavor:string;
    artist:string;
    playerClass:string;
    locale:string;
    img:string;
    imgGold:string;
    mechanichs:Array<string>;
}