import { HsCardCollectionPage } from './app.po';

describe('hs-card-collection App', () => {
  let page: HsCardCollectionPage;

  beforeEach(() => {
    page = new HsCardCollectionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
